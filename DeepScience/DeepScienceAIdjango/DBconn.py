import pandas as pd
import pyodbc


def DBConnection():
    server = '10.0.0.45\SPESQL'
    database = 'DeepScience' 
    username = 'sa' 
    password = 'sa@spe1' 
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return conn


def sp(query):
    conn=DBConnection()
    data = pd.read_sql_query(query,conn)
    df = pd.DataFrame(data)
    columns = []
    for i in df.columns:
        columns.append(i)
    return [df,columns]


def runsql(query):
    conn=DBConnection()
    cursor = conn.cursor()
    cursor.execute(query)
    cursor.commit()

