
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("surveillance.urls")),
    path('accounts/', include('django.contrib.auth.urls')),
    path('UserManager/', include('authentication.urls')),
    path('BreakIn/', include("BreakIn.urls")),
    path('storetiming/', include("storetiming.urls")),
    path('BreakIn', include("BreakIn.urls")),
    path('db/', include("DB.urls")),
    path('PredictiveDash/', include("Predictive.urls")),
    path('api/', include("api.urls")),
    path('storetraffic/', include("StoreTraffic.urls")),
    path('sales/', include("sales.urls")),
    path('configuration/', include("configuration.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
