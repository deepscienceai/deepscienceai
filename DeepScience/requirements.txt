Django
django-auto-logout
django-cors-headers
django-import-export
django-login-required
django-login-required-middleware
django-session-timeout
django-utils-six
djangorestframework
pystan==2.19.1.1
pandas
folium
requests
fbprophet
geocoder

