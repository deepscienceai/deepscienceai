import random
def data_val():
    som=[]
    for i in range(1,100):
        som.append({
            "col":"Traffic Data",
            "idx":i+1,
            "val":i+2,
        })
    data={

"axes": [
    {
    "scale": "x",
    "title": "Time",
    "type": "x"
    },
    {
    "scale": "y",
    "title": "Traffic Data",
    "type": "y"
    }
],
"data": [
    {
    "name": "table",
    "values": 
        som
    }
],
"height": 200,
"legends": [],
"marks": [
    {
    "from": {
        "data": "table",
        "transform": [
        {
            "keys": [
            "data.col"
            ],
            "type": "facet"
        }
        ]
    },
    "marks": [
        {
        "properties": {
            "enter": {
            "stroke": {
                "field": "data.col",
                "scale": "color"
            },
            "strokeWidth": {
                "value": 2
            },
            "x": {
                "field": "data.idx",
                "scale": "x"
            },
            "y": {
                "field": "data.val",
                "scale": "y"
            }
            }
        },
        "type": "line"
        }
    ],
    "type": "group"
    }
],
"padding": "auto",
"scales": [
    {
    "domain": {
        "data": "table",
        "field": "data.idx"
    },
    "name": "x",
    "range": "width"
    },
    {
    "domain": {
        "data": "table",
        "field": "data.val"
    },
    "name": "y",
    "nice": True,
    "range": "height"
    },
    {
    "domain": {
        "data": "table",
        "field": "data.col"
    },
    "name": "color",
    "range": "category20",
    "type": "ordinal"
    }
],
"width": 400
}
    return data