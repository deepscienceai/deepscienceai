# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path
from . import views

urlpatterns = [
    # The home page
    path("",views.kpi_dashboard, name="kpi_dashboard"),
    path("logouts", views.logouts, name="myLogout"),
    path("Surveillance", views.surveillance, name="surveillance"),
    path("get_stores_ajax/", views.get_store_data, name="get_stores_ajax"),
    # path("get_video_stream/", views.get_video_stream, name="get_video_stream"),
    
]
