from django.urls import re_path
from surveillance.views import VideoConsumer

websocket_urlpatterns = [
    re_path(r'ws/video/$', VideoConsumer.as_asgi()),
    re_path(r'ws/video/stop_stream/$', VideoConsumer.as_asgi(action='stop_stream'))
]