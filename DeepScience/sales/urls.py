from os import name
from . import views
from django.urls import path
from . import views
# Predictive , generatePredictive ,exportstorespredict,delete_all_Preddata , dashboard , Date_Market
# from .views import Predictive, generatePredictive ,exportstorespredict ,delete_all_Preddata , dashboard , Date_Market

urlpatterns=[
    path("", views.dashboard , name = "SalesDash"),
    path('Salestable/',views.SalesTable,name='Salestable'),
    # path('generateData/',views.generatePredictive,name='Salestable'),
    path('GeneratePredictivedata/',views.generatePredictive,name='GeneratePredictivedata'),
    # path('exportsalespredict/',views.exportsalespredict , name='exportsalespredict'),
    # path('delete_all_Preddata/', views.delete_all_Preddata , name='delete_all_Preddata'),
    # path('<int:pk>/delete_predictive', views.delete_predictive, name='delete_predictive'),
    # graphs 
    path("filter_Market/", views.filter_Sales_market, name="filter_Sales_Market"),
    path("Market_store_graph/", views.store_month_and_date_graph, name="store_month_and_date_graph"),
    path("Market_and_date/", views.Market_and_date, name="Market_and_date"),
    path("DateGraph/", views.DateGraph, name="month_and_year_Graph"),
    path("year_wise/", views.year_wise, name="year_wise"),
    path("Sales_Table_ajax/", views.Sales_Table_ajax , name="Sales_Table_ajax"),
    path("Export_sales_prediction/", views.Export_sales_prediction , name="Export_sales_prediction"),
]