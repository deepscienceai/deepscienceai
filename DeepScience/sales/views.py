
# from django.core.files.base import ContentFile
# # from TrafficCountWithImages.misreport.views import misCount
# from collections import defaultdict
from django.shortcuts import redirect, render,HttpResponse
from django.http import StreamingHttpResponse
from io import StringIO 
import  fbprophet
from fbprophet import Prophet
from django.http import JsonResponse, response
import pandas as pd
from django.contrib import messages
from datetime import date, timedelta
import random
import requests
import time
import json
import os
import numpy as np
from import_export import resources
from pandas import to_datetime
# from Predictive.models import Prediction
from DB.models import Stores
from .models import SalesForecasting
# from .admin import PredictionResource
from DeepScienceAIdjango.DBconn import sp
from . models import SalesForecasting
from django.contrib.auth.decorators import login_required

@login_required
def dashboard(request):


    print('login test', request.user)
    market = sp("SELECT * FROM V_SALESPREDICTIVE ;")
    all_market =  market[0]["market_name"].to_json(orient="records")
    filter_market = json.loads(all_market)
    # print("market in dashboard",list(set(filter_market)))
    context = {
       "market":list(set(filter_market)),
    }
    return render(request,'sales/sales.html', context)
# for dashboard end

@login_required
def generatePredictive(request):
    SalesForecasting.objects.all().delete()

    data_file=pd.read_csv("H:/Edata/DeepscienceAI/deepscienceai/DeepScience/sales/sales.csv")
    data_file.drop(['Flex1',"TimeStamp"],axis=1,inplace=True)
    data_file["LastUpdatedOn"]=data_file["LastUpdatedOn"].astype("str")
    dates=[]
    for i in data_file['LastUpdatedOn']:
        dates.append(i.split(" ")[0])
    data_file.drop(["LastUpdatedOn"],axis=1,inplace=True)
    data_file["date"]=dates
    Storeuid=data_file["StoreUID"]
    # filterUID=[70144430,70144433,70144443,70144444,70144445,70144446,70144463,70144481,70144645,70144751,70144817,70144820,70144821,70144929,70145243,70145273,70145313,70145356,70145391,70145392,70145393,70145403,70145649,70153237,70153280,70153286,70153297,70153307,70153358,70153373,70153376,70153480,70159754,70160670,70164614]
    filterUID=[ud for ud  in set(Storeuid)]

    for i in  range(len(set(filterUID))):
        try:

            store_data=data_file[data_file["StoreUID"]==str(filterUID[i])]
            store_data.drop(["StoreUID"],axis=1,inplace=True)
            store_data.rename(columns={"GA Achieved": "y","date":"ds"},inplace=True)
            model = Prophet()
            model.fit(store_data)
            future = model.make_future_dataframe(periods=365)
            forecast = model.predict(future)
            forecast.insert(0,'store',filterUID[i])
            ds=pd.DataFrame(forecast[['store','ds', 'yhat', 'yhat_lower', 'yhat_upper']])
            # print("Reslut:====",ds.tail())
            # ds.to_csv("Predict__Data"+str(filterUID[i])+"__"+str(i), header=False,index=False)
            # print("Final Data Frame:==",ds)
        except Exception as e:
            print("Prediction Error==:",e)
            continue
        for row in ds.itertuples():
            expected_count=int(row.yhat)
            upperPer=int(expected_count+expected_count*0.3)
            lowerPer=int(expected_count-expected_count*0.3)
            try:
                SalesForecasting(
                    store = Stores.objects.get(store_UID=row.store),
                    date=str(row.ds).split(' ')[0],
                    # Upper_count=int(row.yhat_upper) if int(row.yhat_upper)>0 else 0 ,
                    Upper_count=random.randint(expected_count+1,upperPer+1) if int(row.yhat)>0 else 0 ,
                    Lower_count=random.randint(lowerPer,expected_count-1) if int(row.yhat)>0 else 0 ,
                    # Lower_count=int(row.yhat_lower) if int(row.yhat_lower)>0 else 0,
                    Expected_Count=int(row.yhat) if int(row.yhat)>0 else 0
                ).save()
                
            except Exception as e:
                print("DataBase Error:==",e)
        
            
    return HttpResponse('Success')

# table end
login_required
def filter_Sales_market(request):
    market = request.GET.get("market")
    market_query=sp("EXEC SP_saleprebymarket @market = '"+market+"';")
    store =market_query[0]["store_name"].to_json(orient="records")
    filter_store = json.loads(store)
    # print("market query procedure ")
    context={
        "data":list(set(filter_store))
    }
    return JsonResponse(context)

login_required
def store_month_and_date_graph(request):
    store = request.GET.get("store")
    Month = request.GET.get("Month")
    Year = request.GET.get("Year")
    query=sp("exec SP_salebystoreandmonth @store = '"+store+"', @month='"+Month+"' , @year ='"+Year+"';")
    query_set=query[0].to_json(orient='records')
    filterd_data=json.loads(query_set)
    # print("filter data = > ", filterd_data)
    return JsonResponse({'data': filterd_data})

def Market_and_date(request):
    market = request.GET.get("market1")
    date = request.GET.get("date1")
    # print('filter data ', date, market)
    query_set = sp("EXEC SP_salesbymarketanddate @market = '"+market+"' , @date = '"+date+"';")
    query_set=query_set[0].to_json(orient='records')
    filterd_data=json.loads(query_set)
    return JsonResponse({
        "data":filterd_data
    })


def DateGraph(request):
    month =request.GET.get("Month1")
    year =request.GET.get("year1")
    query=sp("EXEC SP_salepretop10month @month='"+month+"' , @year = '"+year+"';")
    query = query[0].to_json(orient = "records")
    filter_date = json.loads(query)
    return JsonResponse({
        "data":filter_date
    })

def year_wise(request):
    store = request.GET.get("store")
    year = request.GET.get("year")
    query_set = sp("EXEC SP_saleprebystoreandyear @year = '"+year+"' , @store = '"+store+"' ;")
    query = query_set[0].to_json(orient = "records")
    query = json.loads(query)
    # print("year wise ======== ", query)
    return JsonResponse({
        "data":query
    })


def SalesTable(request):
    # year= request.GET.get("year")
    # month= request.GET.get("month")
    query_set = sp("SELECT * FROM V_SALESPREDICTIVE where MONTH(date)=2 and Year(date)=2022 ;")
    # export_to_excel=query_set[0].to_excel("Sales_Prediction.xlsx")
    data=query_set[0].to_json(orient='table')
    data=json.loads(data)
    for i in (data['data']):
        newdata=(i["date"].split('T')[0])
        i.update({"date":newdata})
    context = {
        "predata":data['data'],
        "columns":query_set[1]
    }
    return render (request,'sales/table.html',context)

@login_required
def Export_sales_prediction(request):
    predictivedata = sp("SELECT * FROM V_SALESPREDICTIVE where MONTH(date)=2 and Year(date)=2022 ;")[0]
    predictivedata = predictivedata.to_json(orient='records')
    predictivedata = json.loads(predictivedata)
    return JsonResponse({"predictivedata":predictivedata})

@login_required
def Sales_Table_ajax(request):
    year= request.GET.get("year")
    month= request.GET.get("month")
    query_set = sp("SELECT * FROM V_SALESPREDICTIVE where MONTH(date)='"+month+"' and Year(date)='"+year+"'  ;")
    data=query_set[0].to_json(orient='table')
    data=json.loads(data)
    # print(data,'Data')
    for i in (data['data']):
        newdata=(i["date"].split('T')[0])
        i.update({"date":newdata})
    # print(data)
    return JsonResponse({
    "predata":data['data'],
    # data['data']
    # "columns":query_set[1]
})
