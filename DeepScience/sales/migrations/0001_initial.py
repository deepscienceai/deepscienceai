# Generated by Django 3.2.11 on 2022-02-02 22:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('DB', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SalesForecasting',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(max_length=100)),
                ('Upper_count', models.CharField(max_length=100)),
                ('Lower_count', models.CharField(max_length=100)),
                ('Expected_Count', models.CharField(max_length=100)),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DB.stores')),
            ],
        ),
    ]
