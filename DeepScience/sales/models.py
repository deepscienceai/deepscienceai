from datetime import date
from os import TMP_MAX
from django.db import models
from django.db.models.base import Model
from django.db.models.fields.related import ForeignKey
from DB.models import Stores



class SalesForecasting(models.Model):
    store=models.ForeignKey(Stores,on_delete=models.CASCADE)
    date=models.CharField(max_length=100)
    Upper_count=models.CharField(max_length=100)
    Lower_count=models.CharField(max_length=100)
    Expected_Count=models.CharField(max_length=100)
    def __str__(self):
        return self.store.store_name