from django.db import models

# Create your models here.
from DB.models import Companies

class Departments(models.Model):
    department=models.CharField(max_length=255,unique=True)

    def __str__(self):
        return self.department


class Designations(models.Model):
    designation=models.CharField(max_length=255,unique=True)

    def __str__(self):
        return self.designation

class Registeration(models.Model):
    EmployeeID=models.CharField(max_length=255,primary_key=True)
    firstName=models.CharField(max_length=255)
    lastName=models.CharField(max_length=255)
    email=models.EmailField(unique=True)
    password=models.CharField(max_length=50)
    department=models.ForeignKey(Department,on_delete=models.CASCADE)
    designation=models.ForeignKey(Designation,on_delete=models.CASCADE)
    company=models.ForeignKey(Companies,on_delete=models.CASCADE)
    registertime=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.EmployeeID

class DSUsers(models.Model):
    email=models.ForeignKey(Registeration,on_delete=models.CASCADE)
    password=models.ForeignKey(Registeration,on_delete=models.CASCADE)


    def __str__(self):
        return self.email.email

 




