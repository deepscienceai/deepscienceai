from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from django.http.response import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.utils import json

from StoreTraffic.models import storetraffic
from .serializer import storetrafficSerializer

from rest_framework.decorators import api_view

from storetiming.models import TimeMonitor
from .serializer import TimeMonitorSerializer

from BreakIn.models import AlertsData
from .serializer import AlertSerializer


# Create your views here.

# Create your views here.
@csrf_exempt
@api_view(['GET', 'POST', 'DELETE'])
def storetraffic_func(request):
    if request.method == 'GET':
        storetraffic_data = storetraffic.objects.all()
        data_serializer = storetrafficSerializer(storetraffic_data, many=True)
        return Response(storetrafficSerializer.data)
    elif request.method == 'POST':
        storetraffic_data = JSONParser().parse(request)
        data_serializer = storetrafficSerializer(data=storetraffic_data)
        if data_serializer.is_valid():
            data_serializer.save()
            return Response(data_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = storetraffic.objects.all().delete()
        return Response({'message': '{} Store traffic data were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
@api_view(['GET', 'POST', 'DELETE'])
def storetiming_func(request):
    if request.method == 'GET':
        storetiming_data = TimeMonitor.objects.all()
        print("What is storetiming_data",storetiming_data)
        
        serializer = TimeMonitorSerializer(storetiming_data, many=True)
        return Response(serializer.data)
        # return Response({'ok':'ok'})
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        storetiming_data = JSONParser().parse(request)
        data_serializer = TimeMonitorSerializer(data=storetiming_data)

        if data_serializer.is_valid():
            data_serializer.save()
            return Response(data_serializer.data, status=status.HTTP_201_CREATED) 
        return Response(data_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    elif request.method == 'DELETE':
        count = TimeMonitor.objects.all().delete()
        return Response({'message': '{} Time Monitor were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@csrf_exempt
@api_view(['GET', 'POST', 'DELETE'])
def alertdata_func(request):
    if request.method == 'GET':
        alerts_data = AlertsData.objects.all()
        print("What is storetiming_data",alerts_data)
        
        serializer = AlertSerializer(alerts_data, many=True)
        return Response(serializer.data)
        # return Response({'ok':'ok'})
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        alerts_data = JSONParser().parse(request)
        data_serializer = AlertSerializer(data=alerts_data)

        if data_serializer.is_valid():
            data_serializer.save()
            return Response(data_serializer.data, status=status.HTTP_201_CREATED) 
        return Response(data_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    elif request.method == 'DELETE':
        count = AlertsData.objects.all().delete()
        return Response({'message': '{} Alert Data were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



