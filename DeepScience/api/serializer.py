from rest_framework import serializers
from StoreTraffic.models import storetraffic
from storetiming.models import TimeMonitor
from BreakIn.models import AlertsData


class storetrafficSerializer(serializers.ModelSerializer):

    class Meta:
        model = storetraffic
        fields = "__all__"

class TimeMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeMonitor
        fields = "__all__"

class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlertsData
        fields = "__all__"
