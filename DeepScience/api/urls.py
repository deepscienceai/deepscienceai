from unicodedata import name
from django.urls import path
from .views import storetiming_func,storetraffic_func,alertdata_func


urlpatterns = [ 
    path('',storetraffic_func, name = 'storetraffic_func' ),
    path("storetiming/",storetiming_func, name = 'storetiming_func'),
    path("alerts/",alertdata_func,),
    # path(r'^api/tutorials$', storetiming_func)
]