import numpy as np
import nltk
# nltk.download('punkt')
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import regexp_tokenize,word_tokenize,sent_tokenize
from nltk.corpus import stopwords
from collections import Counter,deque
stemmer = PorterStemmer()
import sqlite3
from sqlite3 import Error
import pandas as pd
import numpy as np
import random
import json

import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from nltk_utils import bag_of_words, tokenize, stem
from model import BiLSTM_CR


def insert_data(tag,question):
    conn = sqlite3.connect("database.sqlite3")
    cur = conn.cursor()
    select_query=f"SELECT * FROM INTENTS WHERE Tag='{tag}'"
    full_data=pd.read_sql_query(select_query,conn)
    alltags=[t for t in full_data['Tag']]
    if tag in alltags:

        tagdata=full_data[full_data["Tag"]==tag]
        allpattern=[p for p in tagdata['Pattern']]
        if question not in allpattern:
            data_for_insert=(str(tag),str(question))
            insert_query = ''' INSERT INTO INTENTS(Tag,Pattern)
                VALUES(?,?)'''
            cur.execute(insert_query, data_for_insert)
            conn.commit()
    else:
        data_for_insert=(str(tag),str(question))
        insert_query = ''' INSERT INTO INTENTS(Tag,Pattern)
            VALUES(?,?)'''
        cur.execute(insert_query, data_for_insert)
        conn.commit()
    
        
# insert_data('mis','how to communicate with mis team')


def data_get():
    conn = sqlite3.connect("database.sqlite3")
    cur = conn.cursor()
    select_query=f"SELECT * FROM INTENTS"
    full_data=pd.read_sql_query(select_query,conn)
    # alltags=[t for t in full_data['Tag']]
    all_words = []
    tags = []
    xy = []
    # loop through each sentence in our intents patterns
    
    
    # loop through each sentence in our intents patterns
    for intent in range(len(full_data)):
        tag = full_data['Tag']
        # print("All tags in list",tag)
        tags.append(tag)
        for pattern in full_data['Pattern']:
            # tokenize each word in the sentence
            w = tokenize(pattern)
            # print("All pattern after tokin",w)
            # add to our words list
            all_words.extend(w)
            # print("Allwords after extend",all_words)
            # add to xy pair
            xy.append((w, tag))
            # print("All xy after append",xy)

    # stem and lower each word
    ignore_words = ['?', '.', '!']
    all_words = [stem(w) for w in all_words if w not in ignore_words]
    # remove duplicates and sort
    print("All Words",all_words)
    all_words = sorted(set(all_words))
    print("All word",all_words)
    print("tags",tags)
    tags = sorted(set(tags))

    print(xy, "patterns")
    print(len(tags), "tags:", tags)
    print(len(all_words), "unique stemmed words:", all_words)


    # create training data
    X_train = []
    y_train = []
    for (pattern_sentence, tag) in xy:
        # X: bag of words for each pattern_sentence
        bag = bag_of_words(pattern_sentence, all_words)
        X_train.append(bag)
        # y: PyTorch CrossEntropyLoss needs only class labels, not one-hot
        label = tags.index(tag)
        y_train.append(label)

    X_train = np.array(X_train)
    y_train = np.array(y_train)

    # Hyper-parameters 
    num_epochs = 1000
    batch_size = 8
    learning_rate = 0.001
    input_size = len(X_train[0])
    hidden_size = 8
    output_size = len(tags)
    print(input_size, output_size)

    class ChatDataset(Dataset):

        def __init__(self):
            self.n_samples = len(X_train)
            self.x_data = X_train
            self.y_data = y_train

        # support indexing such that dataset[i] can be used to get i-th sample
        def __getitem__(self, index):
            return self.x_data[index], self.y_data[index]

        # we can call len(dataset) to return the size
        def __len__(self):
            return self.n_samples

    dataset = ChatDataset()
    train_loader = DataLoader(dataset=dataset,
                            batch_size=batch_size,
                            shuffle=True,
                            num_workers=0)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = BiLSTM_CR(input_size, hidden_size, output_size).to(device)

    # Loss and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # Train the model
    for epoch in range(num_epochs):
        for (words, labels) in train_loader:
            words = words.to(device)
            labels = labels.to(dtype=torch.long).to(device)
            
            # Forward pass
            outputs = model(words)
            # if y would be one-hot, we must apply
            # labels = torch.max(labels, 1)[1]
            loss = criterion(outputs, labels)
            
            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
        if (epoch+1) % 100 == 0:
            print (f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')


    print(f'final loss: {loss.item():.4f}')

    data = {
    "model_state": model.state_dict(),
    "input_size": input_size,
    "hidden_size": hidden_size,
    "output_size": output_size,
    "all_words": all_words,
    "tags": tags
    }

    FILE = "data.pth"
    torch.save(data, FILE)

    print(f'training complete. file saved to {FILE}')

data_get()