from django.apps import AppConfig


class BreakinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'BreakIn'
