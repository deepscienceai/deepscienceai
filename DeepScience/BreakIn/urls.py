
from django.urls import path
from . import views 

urlpatterns = [
    path("", views.BreakIn, name="BreakIn"),
    path("breakInTable", views.breakInTable, name="btable"),
    path("filter_alerts_Market/", views.filter_alerts_Market , name="filter_alerts_Market"),
    # path("Market_alerts_store_graph/", views.Market_alerts_store_graph , name="Market_alerts_store_graph"),
    # path("Market_and_date_alerts/", views.Market_and_date_alerts , name="Market_and_date_alerts"),
    path("data_history/",views.data_history , name="data_history"),
    path("data_store_history/", views.data_store_history , name="data_store_history")
]