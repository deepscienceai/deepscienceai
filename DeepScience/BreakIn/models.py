from django.db import models
from DB.models import Stores

# Create your models here.
class AlertsData(models.Model):
    store = models.ForeignKey(Stores,on_delete=models.CASCADE)
    alert=models.IntegerField()
    time=models.TimeField()
    date=models.CharField(max_length=50)
    
    def __str__(self):
        return self.store.store_name