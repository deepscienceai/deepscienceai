import json
from sys import api_version
from django.http.response import HttpResponse
from django.shortcuts import render,HttpResponse
from django.http import JsonResponse, response
import pandas as pd
from .models import AlertsData
from DeepScienceAIdjango.DBconn import sp
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def BreakIn(request):
    
    # market = sp("SELECT * FROM V_SALESPREDICTIVE ;")
    store = []
    market=sp("EXEC SP_BREAKMARKET")
    all_market =  market[0]["market_name"].to_json(orient="records")
    filter_market = json.loads(all_market)

    recent=sp("exec SP_breakrecent")

    data =recent[0].to_json(orient="table")
    
    data = json.loads(data) 
    for i in (data['data']):
        newdata=(i["dated"].split('T')[0])
        i.update({"dated":newdata})  

    # print("data====",data)

    context = {
       "market":list(set(filter_market)),
       "data_full":data['data'],
    }
    return render(request,'Breakin_alerts/breakin.html',context)


@login_required
def breakInTable(request):
    return render(request, 'Breakin_alerts/table.html')

@login_required
def filter_alerts_Market(request):
    market = request.GET.get("market")
    month=request.GET.get("month")
    # print("Market:==",market," Month:===",month)
    # print("Market in filter market ->", market)
    market_query=sp("EXEC sp_breakbymarketandmonth @MONTH='"+month+"',@MARKET = '"+market+"';")
    # print("Data========",market_query[0])
    
    query =market_query[0].to_json(orient="records")
    store = json.loads(query)

    context={
        "store":store,
    }
    return JsonResponse(context)

@login_required
def data_history(request):
    market2=request.GET.get("market2")
    stores=sp("EXEC breakiinmarket @market='"+market2+"'")
    storess =  stores[0]["store_name"].to_json(orient="records")
    store_data = json.loads(storess)

    return JsonResponse({
        "data":store_data
    })

@login_required
def data_store_history(request):
    store_1=request.GET.get("store_1")
    market_2= request.GET.get("market_2")

    # print("Market_2",market_2)
    data=sp("EXEC SP_BREAKTOPTWO @STORE = '"+store_1+"', @MARKET='"+market_2+"';")
    all_data =  data[0].to_json(orient="records")
    all_datas = json.loads(all_data)

    history_data=sp("EXEC SP_breakbystore @store ='"+store_1+"'")
 
    data =  history_data[0].to_json(orient="table")
    
    data = json.loads(data)
    # data = json.loads(data) 
    for i in (data['data']):
        newdata=(i["dated"].split('T')[0])
        i.update({"dated":newdata})  


    return JsonResponse({
        "data":[all_datas,data['data']],
        # "history_data":history_alldata
    })



