import pyodbc
import pandas as pd
from pandas import to_datetime
from datetime import datetime,date,timedelta
import datetime
import fbprophet
from fbprophet import Prophet
import random
import schedule
import time




def main ():

    def DBConnection():
        server = '10.0.0.45\SPESQL'
        database = 'DeepScience' 
        username = 'sa' 
        password = 'sa@spe1' 
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        return conn
        
    def runsql(query):
        conn=DBConnection()
        cursor = conn.cursor()
        cursor.execute(query)
        cursor.commit()
        
        
        
    def sp(query):
        conn=DBConnection()
        data = pd.read_sql_query(query,conn)
        df = pd.DataFrame(data)
        columns = []
        for i in df.columns:
            columns.append(i)
        return [df,columns]



    # print (sp("EXEC predictforten;"))
    def daterange():
        dates=[]
        for i in range(0,10):
            x=(datetime.datetime.now()+datetime.timedelta(days=i)).strftime('%Y-%m-%d')
            dates.append(x)
        return dates    

    # print(dates)  
        

    def generatePredictive(sp):
        # sp("EXEC predictforten;")
        runsql("Delete from Predictive_prediction")
        pdata=sp("EXEC predictforten;")
        StoreUid = []
        StoreDate=[]
        ICT=[]
        
        for data in pdata[0]["store_id"]:
            StoreUid.append(int(data))
        for dat in pdata[0]["date"]:
            StoreDate.append((dat))
        for count in pdata[0]["total"]:
            ICT.append(int(count))
            
        allStores=[]
        allStores.append({
                'StoreUids':[data for data in StoreUid],
                'Date':[data for data in StoreDate],
                'TotalCount':[data for data in ICT]
            })
        # print(allStores,'=====')
        
            
            
        
        filterUID=[ud for ud  in set(allStores[0]['StoreUids'])]
        for data in filterUID:
            storedata=allStores[allStores[0]['StoreUids']==data]
            storedata=pd.DataFrame(storedata)
            storedata=(storedata.iloc[1:,1:])
            print (storedata,'storedata')
            try:
                storedata.columns = ['ds', 'y']
                
                storedata['ds']= to_datetime(storedata['ds'])
                model = Prophet()
                # # fit the model  
                model.fit(storedata)
                dates=daterange()
                future = pd.DataFrame(dates)
                future.columns = ['ds']
                future['ds']= to_datetime(future['ds'])
                # # use the model to make a forecast
                forecast = model.predict(future)
                forecast.insert(0,'store',data)
                ds=pd.DataFrame(forecast[['store','ds', 'yhat', 'yhat_lower', 'yhat_upper']])
                print(ds)
                
            except Exception as ex:
                print("exception Occur=====", str(ex))
                continue
                
            for row in ds.itertuples():
                try:
                    store=str(row.store)
                    Expected_Count=int(row.yhat) if int(row.yhat)>0 else 0
                    upperPer=int(Expected_Count+Expected_Count*0.3)
                    lowerPer=int(Expected_Count-Expected_Count*0.3)
                    date=str(row.ds).split(' ')[0],
                    Upper_count=random.randint(Expected_Count+1,upperPer+1) if int(row.yhat)>0 else 0 ,
                    Lower_count=random.randint(lowerPer,Expected_Count-1) if int(row.yhat)>0 else 0 ,
                    print(store,Expected_Count,Upper_count[0],Lower_count[0],date[0])    
                    runsql("exec sp_InsertInPredictive @date='"+str(date[0])+"', @uppercount='"+str(Upper_count[0])+"',@lowwercount='"+str(Lower_count[0])+"',@expectedcount='"+str(Expected_Count)+"',@uid='"+str(store)+"' ")
                except Exception as e:
                    print(e,"error")
                        
    generatePredictive(sp)
                

    
# schedule.every(10).minutes.do(generatePredictive(sp))
schedule.every().tuesday.at('10:00').do(main)
while True:
    schedule.run_pending()
    time.sleep(1)
    # print("ok==============")
