from os import name
from . import views
from django.urls import path
from . import views

urlpatterns=[
    path("", views.dashboard , name = "PredictiveDash"),
    path('Predictivetable',views.Predictive,name='Predictivetable'),
    path('GeneratePredictive',views.generatePredictive,name='GeneratePredictive'),
    path('exportstorespredict/',views.exportstorespredict , name='exportstorespredict'),
    path('delete_all_Preddata/', views.delete_all_Preddata , name='delete_all_Preddata'),
    path('<int:pk>/delete_predictive', views.delete_predictive, name='delete_predictive'),
    # graphs 
    path("filter_Market/", views.filter_market , name="filter_Market"),
    path("Market_store_graph/", views.Market_store_graph , name="Market_store_graph"),
    path("Market_and_date/", views.Market_and_date , name="Market_and_date"),
    path("DateGraph/", views.DateGraph , name="DateGraph"),
] 
