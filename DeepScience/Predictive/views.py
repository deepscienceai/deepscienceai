
# from django.core.files.base import ContentFile
# # from TrafficCountWithImages.misreport.views import misCount
# from collections import defaultdict
import imp
from math import ceil, floor
from django.shortcuts import redirect, render,HttpResponse
import fbprophet
from fbprophet import Prophet
from django.http import JsonResponse, response
import pandas as pd
from django.contrib import messages
from datetime import date, timedelta
import requests
import time
from datetime import date as d
import json
import os
import numpy as np
from import_export import resources
from pandas import to_datetime
from Predictive.models import Prediction
from DB.models import Stores
from .admin import PredictionResource
from DeepScienceAIdjango.DBconn import sp,runsql
import random
from django.contrib.auth.decorators import login_required

# for dashboard
@login_required
def dashboard(request):
    market = sp("EXEC SP_Predictive;")
    all_market =  market[0]["market_name"].to_json(orient="records")
    filter_market = json.loads(all_market)
    
    # print("market in dashboard",list(set(filter_market)))
    context = {
       "market":list(set(filter_market)),
    }
    return render(request,'Predictive/dashboard.html',context)
# for dashboard end

# for table 
@login_required
def Predictive(request):
    pred = sp("EXEC SP_Predictive;")
    pred = pred[0].to_json(orient="records")
    pred = json.loads(pred)
    # print(pred)
    # pred=Prediction.objects.all()
    context={
        'data':pred
    }
    return render(request,'Predictive/table.html',context)

@login_required
def generatePredictive(request):
    # response = requests.get("https://mis.mobilelinkusa.com/deepscienceapi/api/StoreTraffic/getDetail")
    # data = response.json()
    # alldata=(data['Success']['Data'])
    # pdata = pd.read_csv('/home/live/Documents/deepscienceai/DeepScience/Predictive/pred.csv')
    runsql("Delete from Predictive_prediction")
    pdata=sp("EXEC predictforten;")
    # print(pdata)
    StoreUid = []
    StoreDate=[]
    ICT=[]
    allStores=[]
    for data in pdata[0]["store_id"]:
        StoreUid.append(int(data))
    for dat in pdata[0]["date"]:
        StoreDate.append((dat))
    for count in pdata[0]["total"]:
        ICT.append(int(count))

    rangedate=list()

    if request.method=='POST':
        startdate=request.POST.get('startdate')
        enddate=request.POST.get('enddate')
        sdate =  list(map(int, startdate.split("-")))
        sdate=d(*sdate)
        edate = list(map(int, enddate.split("-")))
        edate = d(*edate)
        adate = edate - sdate
        for i in range(adate.days + 1):
            day = sdate + timedelta(days=i)
            rangedate.append(day) 
    # deletdata=Prediction.objects.all()
    # deletdata.delete()
    # #appending all separate data into allstores
    allStores.append({
        'StoreUids':[data for data in StoreUid],
        'Date':[data for data in StoreDate],
        'TotalCount':[data for data in ICT]
    })
    df =pd.DataFrame(allStores[0])
    # print(df,'=====')
    
    filterUID=[ud for ud  in set(StoreUid)]
    for i in range(len(filterUID)):
        storedata=df[df['StoreUids']==filterUID[i]]
        storemean=ceil(storedata['TotalCount'].mean())
        storedata['TotalCount']=storedata['TotalCount'].replace(0,storemean)
        storedata=storedata.iloc[1:,1:]
        
        print(storedata)
        
        # print(storedata,'storedata')
        try:
            storedata.columns = ['ds', 'y']
            
            storedata['ds']= to_datetime(storedata['ds'])
            model = Prophet()
            # # fit the model  
            model.fit(storedata)
            future = pd.DataFrame(rangedate)
            future.columns = ['ds']
            future['ds']= to_datetime(future['ds'])
            # # use the model to make a forecast
            forecast = model.predict(future)
            forecast.insert(0,'store',filterUID[i])
            ds=pd.DataFrame(forecast[['store','ds', 'yhat', 'yhat_lower', 'yhat_upper']])
            
        except Exception as ex:
            print("exception Occur", str(ex))
            continue
        
        
        for row in ds.itertuples():
                try:
                    store=str(row.store)
                    Expected_Count=int(row.yhat) if int(row.yhat)>0 else 0
                    upperPer=int(Expected_Count+Expected_Count*0.3)
                    lowerPer=int(Expected_Count-Expected_Count*0.3)
                    date=str(row.ds).split(' ')[0],
                    Upper_count=random.randint(Expected_Count+1,upperPer+1) if int(row.yhat)>0 else 0 ,
                    Lower_count=random.randint(lowerPer,Expected_Count-1) if int(row.yhat)>0 else 0 ,
                    print(store,Expected_Count,Upper_count[0],Lower_count[0],date[0])    
                    runsql("exec sp_InsertInPredictive @date='"+str(date[0])+"', @uppercount='"+str(Upper_count[0])+"',@lowwercount='"+str(Lower_count[0])+"',@expectedcount='"+str(Expected_Count)+"',@uid='"+str(store)+"' ")
                except Exception as e:
                    print(e,"error")
        # for row in ds.itertuples():
        #     try:
        #         Prediction(
        #             store = Stores.objects.get(store_UID=row.store),
        #             date=str(row.ds).split(' ')[0],
        #             Upper_count=int(row.yhat_upper) if int(row.yhat_upper)>0 else 0 ,
        #             Lower_count=int(row.yhat_lower) if int(row.yhat_lower)>0 else 0,
        #             Expected_Count=int(row.yhat) if int(row.yhat)>0 else 0

        #         ).save()
        #     except Exception as e:
        #         print(e)
        
    return redirect(Predictive)


# exprort all predictive stores

@login_required
def exportstorespredict(request):
    predictivedata = PredictionResource()
    dataset = predictivedata.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="PredictiveStoreCount.csv"'
    return response
# deleting all predicted data
@login_required
def delete_all_Preddata(request):
    alldata=Prediction.objects.all()
    alldata.delete()
    return redirect(Predictive)

@login_required
def delete_predictive(request,pk):
    delete_data=Prediction.objects.get(pk=pk)
    delete_data.delete()
    return redirect("Predictive")
# table end

@login_required
def filter_market(request):
    market = request.GET.get("market")
    market_query=sp("exec SP_PredictivebyMarket @market='"+market+"';")
    store =market_query[0]["store_name"].to_json(orient="records")
    filter_store = json.loads(store)
    context={
        "data":list(set(filter_store))
    }
    return JsonResponse(context)

@login_required
def Market_store_graph(request):
    store = request.GET.get("store")
    query_set = sp("exec SP_Predictivebystores @store ='"+store+"';")
    query_set=query_set[0].to_json(orient='records')
    filterd_data=json.loads(query_set)
    return JsonResponse({'data': [data for data in filterd_data]})

@login_required
def Market_and_date(request):
    market = request.GET.get("market1")
    date = request.GET.get("date1")
    # query_set = sp("exec PredictivebyMarketandDate @market='"+market+"' , @date='"+date+"';")
    query_set = sp("exec SP_PredictivebyMarketandDate @market='"+market+"', @date='"+date+"';")
    query_set=query_set[0].to_json(orient='records')
    filterd_data=json.loads(query_set)
    
    return JsonResponse({
        "data":filterd_data
    })


@login_required
def DateGraph(request):
    date = request.GET.get("date1")
    # query=sp('exec predictivebyDate @date="2021-12-30";')
    query=sp('exec SP_predictivebyDate @date="'+date+'";')
    query = query[0].to_json(orient = "records")
    filter_date = json.loads(query)
    return JsonResponse({
        "data":filter_date
    })