from django.urls import path
from .views import login_view,  register_user,activate
from django.contrib.auth.views import LogoutView

urlpatterns = [
    # path('accounts/login/', login_view, name="accounts/login"),
    path('register/', register_user, name="register"),
    # path("logout/", LogoutView.as_view(), name="logout"),
    path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',activate, name='activate'),
]