# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import LoginForm, SignUpForm
import requests
from django.http import HttpResponse  
from django.shortcuts import render, redirect  
from django.contrib.auth import login, authenticate  
from django.contrib.sites.shortcuts import get_current_site  
from django.utils.encoding import force_bytes, smart_str  
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode  
from django.template.loader import render_to_string  
from .token import account_activation_token  
from django.contrib.auth.models import User  
from django.forms import ValidationError
from django.core.mail import EmailMessage 
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def login_view(request):
    form = LoginForm(request.POST or None)
    msg = None
    if request.method == "POST":
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                msg = 'Invalid credentials'
        else:
            msg = 'Error validating the form'

    return render(request, "accounts/login.html", {"form": form, "msg": msg})


def send_mail(send_from,send_to,subject,cc,bcc,htmlBody,password):
    url = 'https://mis.mobilelinkusa.com/MLServiceAPI/api/SendEmail/SendEmailBody/'
    data = {"from": send_from, "to":send_to,"subject": subject,'cc':cc,'bcc':bcc,'htmlBody':htmlBody,'password':password}
    response = requests.post(url, json=data)


# def info(request, message, fail_silently=False):
#     messages.add_message(request, constants.INFO, message,
#                 fail_silently=fail_silently)


@csrf_exempt
def register_user(request):
    if request.method=='POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user=form.save(commit=False)
            user.is_active=False
            user.save()
             # to get the domain of the current site  
            current_site = get_current_site(request)  
            mail_subject = 'Activation link has been sent to your email id'  
            message = render_to_string('acc_active_email.html', {  
                'user': user,  
                'domain': current_site.domain,  
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),  
                'token':account_activation_token.make_token(user),  
            })  
            to_email = form.cleaned_data.get('email') 
            good_domains = '@mobilelinkusa.com'
            email_domain = to_email.split('@')[-1]
            if email_domain not in good_domains:
                messages.info(request , 'Please Enter Mobilelink Email')
                form = SignUpForm()
                user.delete()
            else:
                print (to_email) 
                send_mail('hasan_khan@mobilelinkusa.com',to_email,mail_subject,'','',message,'mobilelink@2') 
                # return HttpResponse('Please confirm your email address to complete the registration') 
                return render(request, 'accounts/alert.html') 
    else:  
        form = SignUpForm()  
    return render(request, 'accounts/register.html', {'form': form})  


def activate(request, uidb64, token):  
    user = User()
    try:  
        uid = smart_str(urlsafe_base64_decode(uidb64))  
        user = User.objects.get(pk=uid)  
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):  
        user = None  
    if user is not None and account_activation_token.check_token(user, token):  
        user.is_active = True  
        user.save()  
        return redirect (login_view)
    else:  
        return HttpResponse('Activation link is invalid!')  

