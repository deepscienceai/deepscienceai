from logging import exception
from django.shortcuts import render

from DeepScienceAIdjango.DBconn import sp,runsql
import json
from django.http import JsonResponse
import pandas as pd
# Create your views here.
# server_ip module_name  errorstore  notlive
def capture_summary():
    capture_summary = sp("EXEC capture_sum")[0]
    filter_summary_server_4 = capture_summary[capture_summary["server_ip"] == "server_4"]
    total_store = str(list(filter_summary_server_4["total_store"])[0])
    totalLive = str(list(filter_summary_server_4["totalLive"])[0])
    TODAYLive = str(list(filter_summary_server_4["TODAY"])[0])
    server_4_data = str(list(filter_summary_server_4["serverwise"])[0])
    server_ip_4 = str(list(filter_summary_server_4["server_ip"])[0])
    errorstore_4 = str(list(filter_summary_server_4["errorstore"])[0])
    notlive_4 = str(list(filter_summary_server_4["notlive"])[0])
    # filter_ for server 8
    filter_summary_server_8 = capture_summary[capture_summary["server_ip"] == "server_8"]
    server_8_data = str(list(filter_summary_server_8["serverwise"])[0])
    server_ip_8 = str(list(filter_summary_server_8["server_ip"])[0])
    errorstore_8 = str(list(filter_summary_server_8["errorstore"])[0])
    capture_dictionary = {
        'total_store': total_store,
        'totalLive': totalLive,
        'TODAYLive': TODAYLive,
        'server_4_data': server_4_data,
        'server_ip_4': server_ip_4,
        'errorstore_4': errorstore_4,
        'notlive_4': notlive_4,
        'server_8_data': server_8_data,
        'server_ip_8': server_ip_8,
        'errorstore_8': errorstore_8,
    }
    # filter_ for server 8
    return capture_dictionary 
def configuration(request):
    # summary of store traffic  
    data = sp("EXEC strafic_config_summary")[0]
    print("what is the summary of data === ", data)
    # stores of server 7 
    try:  
        total_stores_of_server_7 = data[data["server_ip"] == "server_7"]
        print("what is total stores ", total_stores_of_server_7)
        grand_total_stores = str(list(total_stores_of_server_7['total_stores'])[0])
        totallivestores = str(list(total_stores_of_server_7['totallivestores'])[0])
        data_on_TODAY_in_7 = str(list(total_stores_of_server_7['TODAY'])[0])
        Recent_date = str(list(total_stores_of_server_7['date'])[0])
        total_live_on_server_7 = str(list(total_stores_of_server_7['STOREWISE'])[0])
        Errors_in_7 = int(total_live_on_server_7) - int(data_on_TODAY_in_7)
    except:
        grand_total_stores = 514
        totallivestores =  438
        data_on_TODAY_in_7 = 0
        Recent_date = 0
        total_live_on_server_7 = 0
        Errors_in_7 = 0
    try:
        total_stores_of_sever_3 = data[data["server_ip"] == "server_3"]
        data_on_TODAY_in_3 = str(list(total_stores_of_sever_3['TODAY'])[0])
        total_live_on_server_3 = str(list(total_stores_of_sever_3['STOREWISE'])[0])
        
        Errors_in_3 = int(total_live_on_server_3) - int(data_on_TODAY_in_3)
    except:
        total_stores_of_sever_3 = "Null"
        data_on_TODAY_in_3 = "Null"
        total_live_on_server_3 = "Null"
        Errors_in_3 = "Null"
    # summary of store traffic 

    # summary for store capture
    capture_summary = sp("EXEC capture_sum")[0]
    filter_summary_server_4 = capture_summary[capture_summary["server_ip"] == "server_4"]
    total_store = str(list(filter_summary_server_4["total_store"])[0])
    totalLive = str(list(filter_summary_server_4["totalLive"])[0])
    TODAYLive = str(list(filter_summary_server_4["TODAY"])[0])
    server_4_data = str(list(filter_summary_server_4["serverwise"])[0])
    errorstore_4 = str(list(filter_summary_server_4["serverwise_error"])[0])
    today_data_store_4 = str(list(filter_summary_server_4["serverwise_today"])[0])
    notlive_4 = str(list(filter_summary_server_4["notlive"])[0])
    # filter_ for server 8  
    filter_summary_server_8 = capture_summary[capture_summary["server_ip"] == "server_8"]
    server_8_data = str(list(filter_summary_server_8["serverwise"])[0])
    today_data_store_8 = str(list(filter_summary_server_8["serverwise_today"])[0])
    errorstore_8 = str(list(filter_summary_server_8["serverwise_error"])[0])
    capture_date = str(list(filter_summary_server_8["date"])[0])
    # summary for store capture
    today_total_live_data = int(data_on_TODAY_in_3) + int(data_on_TODAY_in_7)
    conf = {
        'grand_total_stores': grand_total_stores,
        'totallivestores': totallivestores,
        'total_live_on_server_3': total_live_on_server_3,
        "total_live_on_server_7": total_live_on_server_7,
        'data_on_TODAY_in_3': data_on_TODAY_in_3,
        'data_on_TODAY_in_7': data_on_TODAY_in_7,
        'Errors_in_3': Errors_in_3,
        'Errors_in_7': Errors_in_7,
        'Recent_date': Recent_date,
        'today_total_live_data':today_total_live_data,
        # data sumary of capture 
        'total_store': total_store,
        'totalLive': totalLive,
        'TODAYLive': TODAYLive,
        'server_4_data': server_4_data,
        'today_data_store_4': today_data_store_4,
        'errorstore_4': errorstore_4,
        'notlive_4': notlive_4,
        'server_8_data': server_8_data,
        'errorstore_8': errorstore_8,
        'capture_date': capture_date,
        'today_data_store_8': today_data_store_8,
    }
    return render(request, 'configuration/configuration.html',conf)
def store_configuration(request):
    # configuration
    data =sp('select store_name,store_UID from DB_stores where store_uid not in (select store_UID from DB_LiveStore)and IsActive=1 and IsDeleted=0 AND company_id=1')[0]
    snap = sp('select no_of_snap,id from snap')[0]
    thresh = sp('select threshold,id from threshold')[0]
    confidence = sp('select confidence,id from confidence')[0]
    server_ip = sp('select distinct (server_ip),server_id from DB_LiveStore as ls join DB_Servers as ser on ls.server_id=ser.id where module_id=1 and len(store_uid)!=6')[0]
    yolo_module = sp('select yolo_module,id from yolo')[0]
    store_name = data["store_name"]
    snap = snap['no_of_snap']
    thresh = thresh['threshold']
    confidence = confidence['confidence']
    server_i = server_ip['server_ip']
    yolo_module = yolo_module['yolo_module']
    live_stores = sp('EXEC sp_ALLstSTORES')[0]
    print("what is live server_ip ",live_stores )
    query =live_stores.to_json(orient="records")
    live_stores = json.loads(query)
    conf_query = {
        'store_name': store_name,
        'snap': snap,
        'thresh': thresh,
        'confidence': confidence,
        'server_ip': server_i,
        'yolo_module': yolo_module,
        "live_stores":live_stores
    }
    # configuration end
    return render(request, 'configuration/traffic_store.html',conf_query)

def capture_configuration(request):
    # configuration
    # get_live_config_data_by_UID()
    data =sp('select * from Capture_Not_Live_Stores')[0]
    tech = sp('select id,tech_name from DB_dvrlinks')[0]
    channel_list = sp('select  id,chennal from DB_channelslist')[0]
    server_ip=sp ('select distinct (server_ip),server_id from DB_LiveStore as ls join DB_Servers as ser on ls.server_id=ser.id where module_id=2 and len(store_uid)!=6')[0]
    rtsp_user_password = sp('select id,user_name,password from DB_rtsp_user_pass')[0]
    store_name = data["store_name"]
    tech = tech['tech_name']
    rtsp_user = rtsp_user_password["user_name"] 
    channel_list = channel_list['chennal']
    server_i = server_ip['server_ip']
    # joining in pandas 
    server_4 = sp("EXEC SP_server_4")[0]
    server_8 = sp("EXEC SP_server_8")[0]
    frames = [server_4,server_8]
    complete_data = pd.concat(frames)
    query =complete_data.to_json(orient="records")
    complete_data = json.loads(query)
    # joining in pandas 
    conf_query = {
        "Capture_live_stores":complete_data,
        'store_name': store_name,
        'tech': tech,
        'channel_list': channel_list,
        'rtsp_user': set(rtsp_user),
        'server': server_i,
    }

    # configuration end
    return render(request, 'configuration/Capture.html',conf_query)
def retrieve_rtsp_user_password(request):
    rtsp_user_password = sp('select id,user_name,password from DB_rtsp_user_pass')[0]
    get_rtsp_user = request.GET.get('rtsp_user')
    rtsp_password = rtsp_user_password[rtsp_user_password["user_name"]== str(get_rtsp_user)]
    rtsp_password = rtsp_password['password'].to_json(orient="records")
    rtsp_password = json.loads(rtsp_password)
    return JsonResponse({"rtsp_password":rtsp_password})
def retrieve_uid(request):
    store = request.GET.get("store")
    store_UID = sp("select store_UID from DB_stores where store_name='"+store+"'")
    store_UID = store_UID[0]["store_UID"].to_json(orient="records")
    store_UID = json.loads(store_UID)
    return JsonResponse({"storeUID":store_UID})
def retrieve_capture_uid(request):
    store = request.GET.get("store")
    data =sp('select * from Capture_Not_Live_Stores')[0]
    store_UID = data[data['store_name']== store]
    store_UID = store_UID['store_UID'].to_json(orient="records")
    store_UID = json.loads(store_UID)
    return JsonResponse({"storeUID":store_UID})

def get_config_data(request):
    # all sps 
    snap = sp('select no_of_snap,id as si from snap')[0]
    thresh = sp('select threshold,id from threshold')[0]
    confidence = sp('select confidence,id from confidence')[0]
    server_ip = sp('select server_ip,id from DB_Servers')[0]
    yolo_module = sp('select yolo_module,id from yolo')[0]
    st_uid= request.GET.get("storeUID")
    snaps= request.GET.get("snap")
    snaps_filter = snap[snap['no_of_snap'] == int(snaps)]
    threshs= request.GET.get("thresh")
    thresh_filter = thresh[thresh['threshold'] == float(threshs)]
    conf= request.GET.get("confidence")
    conf_filter = confidence[confidence['confidence'] == float(conf)]
    server_ips= request.GET.get("Server")
    server_filter = server_ip[server_ip['server_ip'] == server_ips]
    yolo_modules= request.GET.get("Module")
    yolo_filter = yolo_module[yolo_module['yolo_module'] == yolo_modules]
    snap_fil = str(list(snaps_filter['si'])[0])
    thresh_fil = str(list(thresh_filter['id'])[0])
    conf_fil = str(list(conf_filter['id'])[0])
    server_fil =str(list(server_filter['id'])[0])
    yolo_fil = str(list(yolo_filter['id'])[0])
    inser_data = runsql("EXEC INSERT_IN_STC_LS @UID='"+st_uid+"' ,  @SERVER='"+server_fil+"'  ,  @SNAP= '"+snap_fil+"' ,   @THRES= '"+thresh_fil+"' ,   @CONF= '"+conf_fil+"',   @YOLO='"+yolo_fil+"';")
    if inser_data:
        print("Inserted")
    else:
        print("Not Inserted")
    data = {
        "":""
    }
    return JsonResponse(data)
def get_capture_config_data(request):
    # all sps 
    data =sp('select store_UID,store_name,market_name from Capture_Not_Live_Stores')[0]
    tech = sp('select id,tech_name from DB_dvrlinks')[0]
    channel_list = sp('select  id,chennal from DB_channelslist')[0]
    # server_ip=sp ('select distinct (server_ip),server_id from DB_LiveStore as ls join DB_Servers as ser on ls.server_id=ser.id where module_id=2 and len(store_uid)!=6')
    server_ip = sp('select server_ip,id from DB_Servers')[0]
    rtsp_user_password = sp('select id,user_name,password from DB_rtsp_user_pass')[0]
    stor = request.GET.get("store")
    st_uid= request.GET.get("storeUID")
    technology= request.GET.get("technology")
    channel= request.GET.get("channel")
    username= request.GET.get("username")
    password= request.GET.get("password")
    Server= request.GET.get("Server")
    store_ip= request.GET.get("store_ip")
    # filter_query__for_id
    channel_list_filter = channel_list[channel_list["chennal"]== int(channel)]
    server_filter = server_ip[server_ip['server_ip'] == Server]
    rtsp_user_p_filter =rtsp_user_password[rtsp_user_password["user_name"]==username]
    tech_filter = tech[tech['tech_name']== technology]
    tech_id = str(list(tech_filter["id"])[0])
    channel_list_id = str(list(channel_list_filter["id"])[0])
    server_filter_id = str(list(server_filter["id"])[0])
    rtsp_user_p_id = str(list(rtsp_user_p_filter["id"])[0])
    # filter_query__for_id 
    inser_data = runsql("EXEC insertion_in_rtsp_and_livemodule @store_ip='"+store_ip+"' ,  @dvrlinkid='"+tech_id+"' , @storeuid= '"+st_uid+"' ,   @rtsp_u_p_id= '"+rtsp_user_p_id+"' ,   @channel_list_id= '"+channel_list_id+"',   @servername='"+server_filter_id+"';")
    if inser_data:
        print("Inserted")
    else:
        print("Not Inserted")
    data = {
        "":""
    }
    return JsonResponse(data)

def get_live_config_data_by_UID(request):
    uid = request.GET.get("uid")
    data = sp("EXEC CAPTURE_SERVER4 @UID='"+uid+"'")[0]
    data = data.to_json(orient="records")
    capture_data = json.loads(data)
    print("capture ========== ", capture_data)
    data = {
        "data":capture_data
    }
    return JsonResponse(data)

def update_live_capture_data(request):
    # sql queries 
    techno = sp('select id,tech_name from DB_dvrlinks')[0]
    channel_list = sp('select  id,chennal from DB_channelslist')[0]
    rtsp_user_password = sp('select id,user_name,password from DB_rtsp_user_pass')[0]
    server_ip = sp('select server_ip,id from DB_Servers')[0]
    # sql queries
 
    user_id = request.GET.get("user_id")
    store_ip = request.GET.get("store_ip")
    server = request.GET.get("server")
    tech = request.GET.get("tech")
    channel = request.GET.get("channel")
    print("what is channel ======================================  ", channel)
    password = request.GET.get("password")
    channel_list_filter = channel_list[channel_list["chennal"]== int(channel)]
    server_filter = server_ip[server_ip['server_ip'] == server]
    rtsp_user_p_filter =rtsp_user_password[rtsp_user_password["password"]==password]
    tech_filter = techno[techno['tech_name']== tech]
    # filteration
    tech_id = str(list(tech_filter["id"])[0])
    channel_list_id = str(list(channel_list_filter["id"])[0])
    server_filter_id = str(list(server_filter["id"])[0])
    rtsp_user_p_id = str(list(rtsp_user_p_filter["id"])[0]) 
    # filteration
    runsql("EXEC update_RTSP_LS  @uid='"+user_id+"' ,@up='"+rtsp_user_p_id+"'  , @ip= '"+store_ip+"' , @cha='"+channel_list_id+"',@dvr='"+tech_id+"',@SERID= '"+server_filter_id+"';")
    return JsonResponse({"data":""})
