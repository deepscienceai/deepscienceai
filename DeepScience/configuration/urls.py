from django.urls import path
from . import views
urlpatterns = [
    path('', views.configuration, name="config" ),
    path('store_configuration', views.store_configuration, name="store_configuration" ),
    path('capture_configuration', views.capture_configuration, name="capture_configuration" ),
    path('getStore/', views.retrieve_uid, name="getStore" ),
    path('retrieve_rtsp_password/', views.retrieve_rtsp_user_password, name="retrieve_rtsp_password" ),
    path('get_Capture_Store/', views.retrieve_capture_uid, name="get_Capture_Store" ),
    path('get_config_data/', views.get_config_data, name="get_config_data"),
    path('get_capture_config_data/', views.get_capture_config_data, name="get_capture_config_data"),
    path('get_live_config_data_by_UID/', views.get_live_config_data_by_UID, name="get_live_config_data_by_UID"),
    path('update_live_capture_data/', views.update_live_capture_data, name="update_live_capture_data"),
]