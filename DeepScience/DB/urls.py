from django.contrib import admin
from django.urls import path
from .views import datafeed

urlpatterns = [
    path('', datafeed,name='datafeed'),
]
