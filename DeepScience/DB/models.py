from django.db import models
from django.db.models import manager
from django.db.models.base import ModelState
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField
from django.db.models.fields.related import ForeignKey

# Create your models here.
"""Store Management Tables"""

class Markets(models.Model):
    market_name=models.CharField(max_length=255)

    def __str__(self):
        return self.market_name
class StoreTimingDays(models.Model):
    days=models.CharField(max_length=50)
    def __str__(self):
        return self.days

class StoreTimings(models.Model):
    opening_time=models.TimeField()
    closeing_time=models.TimeField()
    days=models.ForeignKey(StoreTimingDays , on_delete=models.CASCADE)
    def __str__(self):
        return str(self.days)

class TimeZones(models.Model):
    timezone=models.CharField(max_length=50)
    def __str__(self):
        return self.timezone

# Master Table

class Entities(models.Model):
    entity=models.CharField(max_length=255)
    def __str__(self):
        return self.entity


class Companies(models.Model):
    company=models.CharField(max_length=255)
    entity=models.ForeignKey(Entities, on_delete=models.CASCADE)

    def __str__(self):
        return self.company


class Stores(models.Model):
    market_name=models.ForeignKey(Markets,on_delete=models.CASCADE)
    store_name=models.CharField(max_length=255)
    store_UID=models.CharField(max_length=255,primary_key=True)
    ddns=models.CharField(max_length=255,default='N/A')
    timezone=models.ForeignKey(TimeZones,on_delete=models.CASCADE)
    storetime=models.ManyToManyField(StoreTimings)
    company=models.ForeignKey(Companies,on_delete=models.CASCADE,)

    def __str__(self):
        return self.store_name

class Channelslist(models.Model):
    chennal=models.IntegerField()
    def __str__(self):
        return str(self.chennal)

class Rtsp_user_pass(models.Model):
    user=models.CharField(max_length=255)
    password=models.CharField(max_length=255)
    def __str__(self):
        return self.user + ' | ' + self.password
class DVRlinks(models.Model):
    link=models.CharField(max_length=255)
    def __str__(self):
        return(self.link)
class DVR_tech(models.Model):
    techname=models.CharField(max_length=255)
    link =models.ForeignKey(DVRlinks,on_delete=models.CASCADE)
    user_pass=models.ManyToManyField(Rtsp_user_pass)
    def __str__(self):
        return self.techname

#"Master RTSP Table"
class Rtsp(models.Model):
    store=models.ForeignKey(Stores,on_delete=models.CASCADE)
    channels=models.ManyToManyField(Channelslist)
    rtsplink=models.ForeignKey(DVR_tech,on_delete=models.CASCADE)
    ip=models.CharField(max_length=255,default="N/A")

    def __str__(self):
        return self.store.store_name



"""Module Managemenet"""

class Modules(models.Model):
    module_name=models.CharField(max_length=255)
    def __str__(self) :
        return self.module_name


# Live Store Master table
class LiveStores(models.Model):
    store=models.ForeignKey(Stores,on_delete=models.CASCADE)
    module_name=models.ManyToManyField(Modules)
    def __str__(self) :
        return (self.store.store_name)










        




