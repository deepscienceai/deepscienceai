from django.contrib import admin
from . import models

admin.site.register([models.Companies,
models.Entities,
 models.Markets,
 models.Stores,
 models.StoreTimingDays,
 models.StoreTimings,
 models.TimeZones,
 models.Channelslist,
 models.Rtsp_user_pass,
 models.DVR_tech,
 models.Rtsp,
 models.DVRlinks,
 ])

# Register your models here.
