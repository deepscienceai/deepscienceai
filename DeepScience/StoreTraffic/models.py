from django.db import models
from DB.models import Stores
# Create your models here.

class storetraffic(models.Model):
    store = models.ForeignKey(Stores, on_delete=models.CASCADE)
    hourlycount = models.IntegerField()
    hour = models.IntegerField()
    date = models.DateField()
    def __str__(self):
        return self.store.store_name
    class Meta:
        ordering = ['date']