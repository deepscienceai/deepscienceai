from django.urls import path, re_path
from . import views

urlpatterns = [

    # The home page
    path('', views.dashboard, name='dashboard'),
    path('navinclude/', views.dashboard, name='navinclude'),
    path('datatable/', views.table, name='table'),
    path('marketwise_stores/', views.marketwise_stores, name='marketwise_stores'),
    path('Month_and_store/', views.Month_and_store, name='Month_and_store'),
    path('top_ten_stores/', views.top_ten_stores, name='top_ten_stores'),
    path('hourlydata/', views.hourlydata, name='hourlydata'),
    path('hourlyGraph/', views.hourlyGraph, name='hourlyGraph'),
    path('total/', views.total, name='total'),
    path("modal/", views.modal, name="mymodal"),

]

