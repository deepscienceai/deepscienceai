from django.http.response import HttpResponse, JsonResponse
from numpy import record
from .models import storetraffic
from django.shortcuts import render
import json
from DeepScienceAIdjango.DBconn import sp
from authentication.decorators import allowed_users
from django.contrib.auth.decorators import login_required


# Create your views here.
# @allowed_users(allowed_roles=['admin'])
@login_required
def dashboard (request):
    allmarket = sp("select * from DB_markets")
    stores =sp('exec SP_StoreTraffic;')
    query_set = stores[0]["store_name"].to_json(orient="records")
    store = json.loads(query_set)
    context={
        "markets":allmarket[0]["market_name"],
        "stores":list(set(store))
    }
    print('****************************************')
    print(allmarket[0]["market_name"])
    print('****************************************')
    return render(request, "StoreTraffic/dashboard.html", context)

@login_required
def marketwise_stores(request):
    market = request.GET.get("market")
    date = request.GET.get("date")
    query_set = sp('exec SP_stCountbyMarketandDate @market="'+market+'" , @date="'+date+'";')
    data=query_set[0].to_json(orient='records')
    data=json.loads(data)
    return JsonResponse({
        "data":data
    })

@login_required
def Month_and_store(request):
    month = request.GET.get("month")
    store = request.GET.get("store")
    proc = sp("EXEC SP_stcountByMonthandStore @month= '"+month+"', @store = '"+store+"';")
    data=proc[0].to_json(orient='table')

    data=json.loads(data)
    for i in (data['data']):
        newdata=(i["date"].split('T')[0])
        i.update({"date":newdata})
    return JsonResponse({
        "data":data
    })

@login_required
def top_ten_stores(request):
    date = request.GET.get("date")
    proc = sp("exec SP_sttopten @date = '"+date+"';")
    col = proc[0][['store_name','totalcount']]
    data=col.to_json(orient='records')
    data = json.loads(data)
    alldata=data
    return JsonResponse({"proc":alldata})

@login_required
def navinclude (request):
    return render(request, "StoreTraffic/navinclude.html")

@login_required
def table (request):
    markets = sp('exec SP_StoreTraffic;')
    data=markets[0].to_json(orient='table')
    data=json.loads(data)
    for i in (data['data']):
        newdata=(i["date"].split('T')[0])
        i.update({"date":newdata})
    context={
        # "all_data":all_data,
        "markets":data['data'],
        "marketscol":markets[1]
    }
    return render(request, "StoreTraffic/table.html", context)

@login_required
def hourlydata(request):
    stores =sp('exec SP_StoreTraffic;')
    query_set = stores[0]["store_name"].to_json(orient="records")
    store = json.loads(query_set)
    context={
        "stores":list(set(store))
    }
    return render(request, "StoreTraffic/hourlydata.html", context)

@login_required
def hourlyGraph(request):
    store_name = request.GET.get("store")
    date = request.GET.get("date")
    querySet = sp("EXEC SP_stnbydatehour @store = '"+store_name+"' , @date = '"+date+"';")
    hours = querySet[0].to_json(orient = "records")
    hourly = json.loads(hours)
    
    return JsonResponse({
        "data":hourly,
    })
    # return render(request, "")

@login_required
def total(request):
    store_name = request.GET.get("store")
    date = request.GET.get("date")
    # print("store name = ", store_name , " date" , date)
    query = sp("Exec SP_stbystoreanddate @date = '"+date+"', @store = '"+store_name+"';")
    total = query[0].to_json(orient = "records")
    count = json.loads(total)
    # print("what is total count", count[0]["Total"])
    return JsonResponse({
        "totalcount":count[0]["Total"]
    })

def modal(request):
    return render(request, "StoreTraffic/modal.html")