from django.apps import AppConfig


class StoretimingConfig(AppConfig):
    name = 'storetiming'
