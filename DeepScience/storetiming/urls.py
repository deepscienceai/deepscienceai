
from django.urls import path
from .views import TimeMonitors,timemonitorTable,get_charts

urlpatterns = [
    path("", TimeMonitors, name="Timemonitors"),
    path("timemonitorTable/", timemonitorTable, name="tmtable"),
    path("get_charts/", get_charts,name="get_charts")
]