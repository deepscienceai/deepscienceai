from django.db import models
from DB.models import Stores

# Create your models here.
class TimeMonitor(models.Model):
    stores=models.ForeignKey(Stores,on_delete=models.CASCADE)
    date=models.CharField(max_length=100)
    storeopen=models.CharField(max_length=100)
    storeclose=models.CharField(max_length=100)
    morningtime=models.CharField(max_length=100)
    nighttime=models.CharField(max_length=100)
    mtimedifference=models.CharField(max_length=100)
    ntimedifference=models.CharField(max_length=100)
    def __str__(self):
        return str(self.stores.store_name)
