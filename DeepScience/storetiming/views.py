from django.shortcuts import render,HttpResponse
from django.http.response import HttpResponse,JsonResponse
from pandas.core.arrays.sparse import dtype
from .models import TimeMonitor
import json
import pandas as pd
import requests
from DB.models import Stores,Markets
import numpy as np


# Create your views here.
def TimeMonitors(request):
    markets=Markets.objects.all()
    data=TimeMonitor.objects.all()
    market_name=[]
    for i in data:
        market_name.append(i.stores.market_name.market_name)
    market_name=list(set(market_name))

    return render(request,'storetiming/storetiming.html',{'markets':market_name})


def timemonitorTable(request):
    data=TimeMonitor.objects.all()
    
    values={
        "data":data,
    }

   
    return render(request, 'storetiming/table.html',values)

def dataforchart():
    # Stores
    data=TimeMonitor.objects.all()
    market_name=[]
    store_name=[]
    store_UID=[]
    date=[]
    store_open=[]
    store_close=[]
    morning_time=[]
    night_time=[]
    mtime_dif=[]
    ntime_dif=[]

    for i in data:
        market_name.append(i.stores.market_name.market_name)
        store_name.append(i.stores.store_name)
        store_UID.append(i.stores_id)
        date.append(str((i.date)))
        store_open.append(i.storeopen)
        store_close.append(i.storeclose)
        morning_time.append(i.morningtime)
        night_time.append(i.nighttime)
        mtime_dif.append(i.mtimedifference)
        ntime_dif.append(i.ntimedifference)
    
    all_data=pd.DataFrame(list(zip(market_name,store_name,store_UID,date,store_open,store_close,morning_time,night_time,mtime_dif,ntime_dif)),
    columns=['Market_Name','Store_Name','Store_UID','Date','Store_Open','Store_Close','Morning_Time','Night_Time','MTime_Diff','NTime_Diff'])
    
    return all_data


def get_charts(request):
    data=dataforchart()
    market_seletd = request.GET.get('market')
    print("market=====:",type(market_seletd),"What is Market Selected:======",market_seletd)
    date_selected = request.GET.get('date')
    filtered_values = np.where((data['Market_Name']==market_seletd) & (data['Date']== date_selected))
    result_data=data.loc[filtered_values]
    store_name=result_data["Store_Name"]
    # print("Store Names===========",store_name) #Show on X Axis
    store_open_time_line=result_data["Store_Open"] # Show Line in graph where Time is Seperated
    
    def create_line(open,line):
        x=[]
        for i in range(0,len(line)):
            
            x.append(open)
        return x
    store_open_time_line=create_line(9,store_name)
    # print("For Create Line",store_open_time_line)
    store_close_time_line=result_data["Store_Close"]
    store_close_time_line=create_line(20,store_name)
    # print("Fore Close Creat Line",store_close_time_line)
    store_open=result_data["Store_Open"]
    store_close=result_data["Store_Close"]

    def store_time_split(value):
        x=[]
        for i in value:
            value=i.split(":")[0]
            x.append(value)
        time=[]
        for i in x:
            if int(value)<=9:
                time.append(int(value[1]))
            elif int(value)>9:
                time.append(int(value))
        return time
    
    store_early_open=store_time_split(store_open)
    store_early_close=store_time_split(store_close)

    # print("store Early Open: ",store_early_open)
    # print("Store Early Close: ",store_early_close)

    def late_time(value):
        for i in value:
            late=[]
            if i>9 and i<12:
                late.append(i)
            elif i>20:
                late.append(i)
        return late
    late_open=late_time(store_early_open)
    late_close=late_time(store_early_close)
    # print("Late_Open:",late_open)
    # print("Late_close:",late_close)

    stores_name=result_data["Store_Name"]
    store_name=[]
    for i in stores_name:
        
        store_name.append(i)

    # print("Stores _name========",store_name)
        
    
    data = {
        "store_name":store_name,
        'sctline':store_close_time_line,
        "sotline":store_open_time_line,
        "seopen":store_early_open,
        "seclose":store_early_close,
        "slopen":late_open,
        "slclose":late_close,
    }
    return JsonResponse(data)


