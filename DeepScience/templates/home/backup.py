# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import template
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, request
from django.template import loader
from django.urls import reverse
from django.shortcuts import render
import numpy as np
from datetime import datetime
import folium
import requests
import pandas as pd
import json
import requests
from DeepScienceAIdjango.DBconn import sp
from django.http import JsonResponse 
from .datafiles import data_val
import cv2
from django.views.decorators import gzip
from django.http import StreamingHttpResponse
import threading
import time
@login_required
def kpi_dashboard(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('login'))
    
    # def long_and_lat(name):
    #     lnge=geocoder.arcgis(name).lng
    #     latu =  geocoder.arcgis(name).lat
    #     return lnge,latu    
    def market_total_count(values):
        market=values[0]
        market = market.to_json(orient = "records")
        
        popup_message=str(market[5:-2])
        return popup_message
         
    markets_name=list(['Connecticut','Wisconsin - East','Texas - South','Illinois',
    'Wisconsin - West','Laredo','Las Vegas','Texas West'])
    
    total_count=[sp("exec SP_MarketTotalCount @market ='"+data+"'") for data in markets_name ]
    
    
    
    popup_messages=[]
    for i in total_count:
        popup_messages.append(market_total_count(i))
    
    # long_and_lat("Connecticut")
    conn_lng,conn_latu=-72.73830590799997,41.57350273000003
    
    # long_and_lat('Wisconsin - East')

    wille_lng,wille_latu=-88.94502932262965,43.94819918241662
    

    # long_and_lat('Texas - South')
    texass_lng,texass_latu=-98.50882399999995,28.942356000000075
  
    
    # long_and_lat('Illinois')
    ill_lng,ill_latu=-89.15865090199998,40.11404657000003
   
    
    # long_and_lat('Houston TX')
    # hous_lng,hous_latu= -95.36951999999997,29.760780000000068
    
    
    # long_and_lat('Wisconsin - West')
    willw_lng,willw_latu=-89.66080999999997,44.97216000000003
    
    
    # long_and_lat('Laredo')
    lare_lng,lare_latu=-99.50222999999994 ,27.530920000000037
    
    
    # long_and_lat('Las Vegas')
    lv_lng,lv_latu=-115.14000999999996,36.17193000000003
   
    
    # long_and_lat('Texas West')
    tw_lng,tw_latu=-97.09202999999997 ,31.80251000000004
    

    tooltip = "Traffic Count"




    map1=folium.Map( zoom_start=50,tiles="cartodbpositron")
    folium.Marker(
    location=[conn_latu, conn_lng],
    popup=f'<i> Connecticut: {popup_messages[0]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[wille_latu, wille_lng],
    popup=f'<i> Wisconsin - East: {popup_messages[1]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[texass_latu, texass_lng],
    popup=f'<i> Texas - South: {popup_messages[2]} <i/>' ,
    tooltip=tooltip).add_to(map1)
    
    folium.Marker(
    location=[ill_latu, ill_lng],
    popup=f'<i> Illinois: {popup_messages[3]} <i/>' ,
    tooltip=tooltip).add_to(map1)
   
    # folium.Marker(
    # location=[hous_latu,hous_lng],
    # popup=f'<i> Houston TX: {popup_messages[4]} <i/>' ,
    # tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[willw_latu, willw_lng],
    popup=f'<i> Wisconsin - West: {popup_messages[4]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[lare_latu, lare_lng],
    popup=f'<i> Laredo: {popup_messages[5]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[lv_latu, lv_lng],
    popup=f'<i> Las Vegas: {popup_messages[6]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    folium.Marker(
    location=[tw_latu, tw_lng],
    popup=f'<i> Texas West: {popup_messages[7]} <i/>' ,
    tooltip=tooltip).add_to(map1)

    map1=map1._repr_html_()

    predictive_total_count=sp("EXEC SP_predictiveTotalCount")
    predictive_total_count=market_total_count(predictive_total_count)

    store_total_count=sp("Exec SP_StTotalCount")
    store_total_count=store_total_count[0]["TOTALCOUNT"].iloc[0]

    live_total_store=sp("exec SP_TotalLiveStore")
    live_total_store=market_total_count(live_total_store)
    
    stcd=sp("exec SP_stbydateandcount")
    stcd=stcd[0]
    stc=stcd["TOTALCOUNT"]
    std=stcd["date"]
    std_split=[]
    for i in std:
        std_split.append(int((str(i).split("-")[2])))
    stc_li=[]
    for i in stc:
        stc_li.append(i)

    total_live_=sp("EXEC SP_totallivebytimezone")
    total_live_=total_live_[0]
    zone=total_live_["timezone"]

    market_=total_live_["MARKET"]
    market__=[]
    for i in market_:
        market__.append(i)
    zone_=[]
    for i in zone:
        if i=="CTZ":
            zone_.append("Central")
        elif i=="ETZ":
            zone_.append("Eastern")
        else:
            zone_.append("Pacific")

    context={
        "map1":map1,
        "predict":predictive_total_count,
        "storetotalc":store_total_count,
        "livets":live_total_store,
        "store_count_date":list(std_split),
        "store_count":list(stc_li),
        "zone":zone_,
        "market":market__
    }
    return render(request, 'home/kpi_dashboard.html',context)

    # html_template = loader.get_template('home/dashboard.html')
    # return HttpResponse(html_template.render(context, request))



def logouts(request):
    from django.core.cache import cache
    cache.clear()
    return HttpResponseRedirect(reverse('logout'))



@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:
        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        html_template = loader.get_template('Breakin_alerts/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))

    
def surveillance(request):
    from django.core.cache import cache
    cache.clear()

    Market = sp("EXEC markets;")
    marks=Market[0].to_dict(orient='record')
    context = {
        'marks': marks,
    }

    return render(request, 'home/surveillance.html', context)


def get_store_data(request):
        id = request.GET.get('id')
        store = sp("exec SP_Store @id="+str(id)+";")
        store = store[0].to_dict(orient='record')
        return JsonResponse(store, safe=False)


is_video_running = True
threads = False
class VideoCamera(object):
    def __init__(self, is_continued, market=None, store=None, date=None, channel=None, start_time=None, end_time=None):
        self.market=market
        self.store=store
        self.date=date
        self.channel=channel
        self.start_time=start_time
        self.end_time=end_time
        self.is_continued = is_continued
        print('calling init.....', is_continued)
        if is_continued:
            self.start()
        else:
            self.is_continued()

    def start(self):
         # Convert a time string to a datetime object        
        start_time = datetime.strptime(f"{self.date} {self.start_time}:00", "%Y-%m-%d %H:%M:%S")
        end_time = datetime.strptime(f"{self.date} {self.end_time}:00", "%Y-%m-%d %H:%M:%S")

        # Convert the datetime object to the desired format
        formatted_start_time = start_time.strftime("%Y%m%dT%H%M%SZ")
        formatted_end_time = end_time.strftime("%Y%m%dT%H%M%SZ")

        # Getting data from DB 
        store_data = sp(f"exec Sp_playback @market_id='{str(self.market)}', @storeuid='{str(self.store)}';")
        store_credentional=store_data[0].to_dict(orient='record')

        store_ip = store_credentional[0].get('store_ip')
        user_name = store_credentional[0].get('user_name')
        password = store_credentional[0].get('password')        
        
        self.video = cv2.VideoCapture(f'rtsp://{user_name}:{password}@{store_ip}:8554/streaming/tracks/{self.channel}?starttime={formatted_start_time}?endtime={formatted_end_time}')
        (self.grabbed, self.frame) = self.video.read()
        self.started_thread = threading.Thread(target=self.update, args=()).start()

    def stop_stream(self):
        self.started_thread.join()

        if not self.is_continued:
            update()

    def __del__(self):
        print('deleting....')
        self.video.release()

    def get_frame(self):
        image = self.frame
        _, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

    def update(self):
        while self.is_continued:
            time.sleep(1)
            (self.grabbed, self.frame) = self.video.read()

def gen(camera):
    while is_video_running:
        frame = camera.get_frame()
        yield(b'--frame\r\n'
              b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@gzip.gzip_page
def StartStreamRequest(request): 
    data = request.GET
    market = data.get('market')
    store = data.get('storesuid')
    date = data.get('date')
    channel = data.get('channel')
    start_time = data.get('start_time')
    end_time = data.get('end_time')
    

    try:
        cam = VideoCamera(True, market, store, date, channel, start_time, end_time)
        return StreamingHttpResponse(gen(cam), content_type="multipart/x-mixed-replace;boundary=frame")
    except Exception as ex:  
        print(ex)
        pass

def stopStreamingRequest(request):
    try:
        print('requesting stoing ')
        
        VideoCamera(False)
    except Exception as ex:
        print(ex)
        
    return JsonResponse({"stoped": "ok"})
    
    
    
    
    
    
    
    
    
    
        
    
         
    



















# def appDash(request):
#     return render(request, 'Breakinalerts/breakin.html')

# def breakInTable(request):
#     return render(request, 'Breakinalerts/table.html')